import glob
import os
import re
import zipfile

import requests
from lxml import html
from tqdm import tqdm

ARCHIVE_PATH = "https://archive.sensor.community/csv_per_month/"
DATA_PATH = "data/"
FILTER = "2021\-12\_bme280"


def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, "wb") as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)


if __name__ == "__main__":

    if not os.path.exists(os.path.join(DATA_PATH, "csv_per_month/raw")):
        os.makedirs(os.path.join(DATA_PATH, "csv_per_month/raw"))

    if not os.path.exists(os.path.join(DATA_PATH, "csv_per_month/csv")):
        os.makedirs(os.path.join(DATA_PATH, "csv_per_month/csv"))

    download_links = []
    main_page = requests.get(ARCHIVE_PATH)
    main_page_parsed = html.fromstring(main_page.content)
    child_pages = [p for p in main_page_parsed.xpath("//a/@href") if re.match("\d{4}\-\d{2}", p)]
    for child_page in tqdm(child_pages, desc="prepare list of download links"):
        links_page = requests.get(ARCHIVE_PATH + child_page)
        links_page_parsed = html.fromstring(links_page.content)
        links = [child_page + l for l in links_page_parsed.xpath("//a/@href") if re.match(".+\.zip$", l)]
        download_links.extend(links)

    # download raw files
    for download_link in tqdm(
        [l for l in download_links if re.match(FILTER, l.split("/")[-1])], desc="download files"
    ):
        file_path = os.path.join(DATA_PATH, "csv_per_month/raw", download_link.split("/")[-1])
        full_download_link = ARCHIVE_PATH + download_link
        if not os.path.exists(file_path):
            download_url(full_download_link, file_path)

    # unzip raw files
    for file_path in tqdm(glob.glob(os.path.join(DATA_PATH, "csv_per_month/raw/*")), "unzip"):
        with zipfile.ZipFile(file_path, "r") as zip_ref:
            # assuming that we have only one file in each zip archive
            if not os.path.exists(os.path.join(DATA_PATH, "csv_per_month/csv", zip_ref.namelist()[0])):
                zip_ref.extractall(os.path.join(DATA_PATH, "csv_per_month/csv"))
