import os

import dask.dataframe as dd
import h3

RAW_DATA_PATH = "data/csv_per_month/csv"
PROCESSED_DATA_PATH = "data/csv_per_month/parquet"
AGGREGATES_DATA_PATH = "data/aggregates/2021_15min_bme280"
FILTER = "2021-12*bme*"


if __name__ == "__main__":
    # load raw data
    df_ = dd.read_csv(
        os.path.join(RAW_DATA_PATH, FILTER),
        sep=";",
        dtype={"temperature": "object", "pressure": "object", "humidity": "object"},
    )

    # filter Moscow region
    df_ = df_[(df_["lat"].between(55, 56)) & (df_["lon"].between(37, 38))]

    # handle some values
    df_ = df_[
        (~df_["temperature"].isin(["unknown", "unavailable"]))
        & (~df_["pressure"].isin(["unknown", "unavailable"]))
        & (~df_["humidity"].isin(["unknown", "unavailable"]))
    ]

    # preprocess dtypes and add key for partitioning
    df_["temperature"] = df_["temperature"].astype("float32")
    df_["pressure"] = df_["pressure"].astype("float32")
    df_["humidity"] = df_["humidity"].astype("float32")
    df_["timestamp"] = dd.to_datetime(df_["timestamp"])
    df_["date"] = df_["timestamp"].dt.strftime("%Y-%m-%d")

    df_ = df_[["timestamp", "date", "lat", "lon", "pressure", "temperature", "humidity"]]

    # save data to parquet
    df_.to_parquet(PROCESSED_DATA_PATH, compression="gzip", engine="fastparquet", partition_on="date", overwrite=True)

    # make aggregates
    df_ = dd.read_parquet(PROCESSED_DATA_PATH)
    df_["polygon6_id"] = df_.apply(lambda x: h3.geo_to_h3(x["lon"], x["lat"], 6), axis=1)
    df_["timestamp"] = df_["timestamp"].dt.round("15T")  # reduce precision

    # calculate aggregates
    df_agg_ = (
        df_.groupby(["timestamp", "polygon6_id"])
        .agg({"pressure": "mean", "temperature": "mean", "humidity": "mean"})
        .reset_index()
    )

    # save aggregates
    df_agg_.to_parquet(AGGREGATES_DATA_PATH, compression="gzip", engine="fastparquet", overwrite=True)
