# breathe
Sample project with examples on working with geo-data

- ETL (load data from source, transform, aggregate)
- EDA and plotting prototype with `folium`

![map](map.png)

## More about project
- [Data source](https://archive.sensor.community/csv_per_month/)
- [Inspirational video from Yandex team](https://www.youtube.com/watch?v=mh9BlrnktEE) - rewrite ideas from video from Yandex.cloud platform (`Hadoop` + `Spark` + `Clickhouse` + `Datalens`) to simple yet easy scalable stack (`Dask` + `pandas` + `folium`)
- Main target - understand main principles of working with geo data (especially working with `H3` and manipulation with hex-polygons)

## Remarks
- Examples operate with filtered data from Moscow region
- It's only prototype - for example `yaml` configs are only placeholders, but in future I prefer to use `Hydra` for configs orchestration

## Pipeline
```shell
python scripts/load.py
python scripts/process.py
```

Open `notebooks/map.ipynb`

## Install dependencies
```shell
# main dependencies
pip install -r requirements/main.txt

# dev dependencies
pip install -r requirements/dev.txt
```

## Contribution guide
1. Install dev dependencies
2. Install git hook script `pre-commit install`
3. Read contribution guide (`link to WIKI`)
